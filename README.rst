=======================
Django-project-template
=======================

A project template for Django 1.4 

Creating a project
==================

1. Create virtualenv and activate::

    $ cd ~/.virtualenvs
    $ mkvirtualenv {{ env_name }}
    $ source {{ env_name }}/bin/activate
 
2. Install Django and South::

    $ {{ env_name }} pip install django

3. Create Django project with the template::
    
    $ {{ env_name }} django-admin.py startproject --template https://bitbucket.org/domask/django-project-template/get/master.zip --extension py,rst {{ project_name }}

4. Install dependencies

    Development::

        $ {{ env_name }} pip install -r requirements/local.txt

    Production::

        $ {{ env_name }} pip install -r requirements.txt

5. Adjust settings, path and set environmental variables in .bashrc
    
    Development::

        $ export DB_USER
        $ export DB_PASS
        $ add2virtualenv `pwd`

    Production::
    
        $ export DB_USER
        $ export DB_PASS
        $ export SECRET_KEY
        $ add2virtualenv `pwd`

6. Initialize git repository::

    $ {{ env_name }} git init

7. Run the project::
    
    $ {{ env_name }} django-admin.py syncdb --settings={{ project_name }}.settings.local
    $ {{ env_name }} django-admin.py runserver --settings={{ project_name }}.settings.local

    or 

    $ export DJANGO_SETTINGS_MODULE={{ project_name }}.settings.local (preferably in .bashrc)
    $ {{ env_name }} django-admin.py syncdb
    $ {{ env_name }} django-admin.py runserver
