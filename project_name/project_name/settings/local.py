from os.path import join, normpath
from base import *

############### DEBUG CONFIG
DEBUG = True

TEMPLATE_DEBUG = DEBUG
############### END DEBUG CONFIG


############### DATABASE CONFIG
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.',
        'NAME': '',
        'USER': get_env_setting('DB_USER'),
        'PASSWORD': get_env_setting('DB_PASS'),
        'HOST': '',
        'PORT': '',
    }
}
############### END DATABASE CONFIG


############### APPS CONFIG
# Note: only for development or testing purposes
INSTALLED_APPS += (
)
############### END APPS CONFIG
